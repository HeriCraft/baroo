package beans.bases;

public abstract class Animal {
    private String nom;
    private String sexe;
    private String color;
    private double poids;
    private double posX;
    private double posY;

    public String getNom(){
        return this.nom;
    }
    public void setNom(String nom){
        this.nom = nom;
    }

    public String getSexe(){
        return this.sexe;
    }
    public void setSexe(String sexe){
        this.sexe = sexe;
    }

    public String getColor(){
        return this.color;
    }
    public void setColor(String color){
        this.color = color;
    }
    
    public double getPoids(){
        return this.poids;
    }
    public void setPoids(double pd){
        this.poids = pd;
    }

    private double getPosX(){
        return this.posX;
    }
    private void setPosX(double px){
        this.posX = px;
    }
    private double getPosY(){
        return this.posY;
    }
    private void setPosY(double px){
        this.posY = px;
    }
}