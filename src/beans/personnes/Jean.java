package beans.personnes;

import java.sql.Date;

import beans.bases.Personne;

public class Jean extends Personne {
    public Jean(){

    }

    public Jean(String nom, String sex, Date dt, Personne[] fr){
        this.setNom(nom);
        this.setSexe(sex);
        this.setDn(dt);
        this.setAmis(fr);
    }
    public Jean(String nom, String sex, Date dt){
        this.setNom(nom);
        this.setSexe(sex);
        this.setDn(dt);
    }
}